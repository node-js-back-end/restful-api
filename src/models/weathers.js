const mongoose=require ('mongoose')

const weatherSchema =new mongoose.Schema({
    ranking:{
        type:Number,
        required:true,
        unique:true
    },
    city_Name:{
        type:String,
        required:true,
        trim:true
    },
    city_State:{
        type:String,
        required:true,
        trim:true
    },
    id:{
        type:Number,
        required:true,
        unique:true
    },
    date:{
        type:Number,
        required:true
    }
})
//we are creating a new collections
const WeatherRank = new mongoose.model("WeatherRank",weatherSchema);

module.exports = WeatherRank;

