 const express = require('express')
 const path = require('path')

require("../src/db/conn")
 const app= express()
const weather =require("../src/models/weathers")


const port = process.env.port || 3001

app.use(express.json())



app.post("/weather",async(req,res) => {
    try{
        const addWeatherRecords = new weather(req.body)
        console.log(req.body)
        const insertweather =  await addWeatherRecords.save()
        res.status(201).send(insertweather)

    }catch(e){
        res.status(400).send(e)

    }
})

app.get("/",async(req,res) =>{
    res.send("hello u r in............")
})

app.get("/weather",async(req,res) => {
    try{
        const getweather= await weather.find({}).sort({"ranking":1})
        res.status(201).send(getweather)

    }catch(e){
        res.status(400).send(e)

    }
})
// get individual data from collections
app.get("/weather/:id",async(req,res) => {
    try{
        const _id = req.params.id
        const getweather_1= await weather.findById({_id})
        res.status(201).send(getweather_1)
        console.log(getweather_1)

    }catch(e){
        res.status(400).send(e)

    }
})

//update the single one
app.patch("/weather/:id",async(req,res) => {
    try{
        const _id = req.params.id
        const getweather_1= await weather.findByIdAndUpdate(_id,req.body , {
            new : true
        })
        res.status(201).send(getweather_1)
        //console.log(getweather_1)

    }catch(e){
        res.status(400).send(e)

    }
})

// delete operations
app.delete("/weather/:id",async(req,res) => {
    try{
        
        const getweather_1= await weather.findByIdAndDelete(req.params.id)
        res.status(201).send(getweather_1)
        //console.log(getweather_1)

    }catch(e){
        res.status(400).send(e)

    }
})

app.listen(port , () =>{
    console.log(`connection is live at port no. ${port}`)
})